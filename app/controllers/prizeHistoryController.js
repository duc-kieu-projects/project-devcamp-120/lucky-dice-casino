const mongoose = require('mongoose');
const prizeHistoryModel = require('../models/prizeHistoryModel');
const userModel = require('../models/userModel');

const prizeHistoryController = {
    //POST new prize history
    createPrizeHistory: (req,res) => {
        //B1: cbi dữ liệu
        let bodyReq = req.body;
        let newPrizeHistory = {
            _id: mongoose.Types.ObjectId(),
            user: bodyReq.user,
            prize: bodyReq.prize
        }
        //B2: kiểm tra dữ liệu
        if (!(bodyReq.user || bodyReq.prize)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'User or Prize are invalid'
            })
        };

        //B3: thao tác với CSDL
        prizeHistoryModel.create(newPrizeHistory, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error : ${err.message}`
                })
            } else {
                return res.status(201).json({
                    status: 'New Prize History has been created!',
                    'New Prize History': data
                })
            }
        })
    },

    //GET all prizes history
    getAllPrizesHistory: (req,res) => {
         //B1: cbi dữ liệu
         let userId = req.query.user;
         let condition = {};
         if (userId) {
             condition.user = userId
         };
         console.log(condition);

        prizeHistoryModel.find(condition, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal Server error : ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET All prizes history',
                    'Prize History List': data
                })
            }
        }).populate('prize');
    },

    //GET a prize history by ID
    getPrizeHistoryById: (req,res) => {
        //B1: cbi dữ liệu
        let prizeHistoryId = req.params.prizeHistoryId;
        //B2: ktra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
            return res.status(400).json({
                message: `Prize History ID is invalid`
            })
        } else {
            prizeHistoryModel.findById(prizeHistoryId, (err,data) => {
                if (err) {
                    return res.status(500).json({
                        message: `Internal Server error : ${err.message}`
                    })
                } else {
                    return res.status(200).json({
                        status: 'GET a prizes history',
                        'Prize History Found': data
                    })
                }
            })
        }
    },

    //GET prize history by Username
    getPrizeHistoryByUsername: (req,res) => {
        //B1: cbi dữ liệu
        let username = req.query.username;
        console.log(username);
        //B2: ktra dữ liệu
        if (!username) {
             prizeHistoryModel.find( (err,data) => {
                 if (err) {
                     return res.status(500).json({message: `Internal server error: ${err.message}`})
                 } else {
                     return res.status(200).json({
                         status: 'GET all prizes histories',
                         'Prize Histories List': data
                     })
                 }
             })
        } else {
         userModel.findOne({ username }, (errFind, userExist) => {
             if (errFind) {
                prizeHistoryModel.find( (err,data) => {
                     if (err) {
                         return res.status(500).json({message: `Internal server error: ${err.message}`})
                     } else {
                         return res.status(200).json({
                             status: 'GET all prizes histories',
                             'Prize Histories List': data
                         })
                     }
                 })
             } else {
                if (!userExist) {
                   return res.status(200).json({
                        status: 'Username not found',
                        'Prize History': []
                   })
                } else {
                    prizeHistoryModel.find( { user : userExist._id }, (err,data) => {
                        if (err) {
                            return res.status(500).json({message: `Internal server error: ${err.message}`})
                        } else {
                            console.log(username._id)
                            return res.status(200).json(data)
                        }
                    }).populate('prize');
                }
             }
         })
        }
     },

    //PUT a prize history by ID
    updatePrizeHistoryById: (req,res) => {
        //B1: cbi dữ liệu
        let prizeHistoryId = req.params.prizeHistoryId;
        let bodyReq = req.body;
        let updatePrizeHistory = {
            user: bodyReq.user,
            prize: bodyReq.prize
        }
        //B2: ktra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
            return res.status(400).json({
                message: `Prize History ID is invalid`
            })
        } else {
        //B3: thao tác với CSDL
            prizeHistoryModel.findByIdAndUpdate(prizeHistoryId, updatePrizeHistory, (err,data) => {
                if (err) {
                    return res.status(500).json({
                        message: `Internal server error : ${err.message}`
                    })
                } else {
                    return res.status(200).json({
                        status: 'Prize History has been updated!',
                        'Prize History': data,
                        'Data updated': updatePrizeHistory
                    })
                }
            })
        }
    },

    //DELETE a prize history by ID
    deletePrizeHistoryById: (req,res) => {
        //B1: cbi dữ liệu
        let prizeHistoryId = req.params.prizeHistoryId;
         //B2: ktra dữ liệu
         if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
            return res.status(400).json({
                message: `Prize History ID is invalid`
            })
        } else {
        //B3: thao tác với CSDL
        prizeHistoryModel.findByIdAndDelete(prizeHistoryId, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error : ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'Prize History has been deleted!',
                })
            }
            })
        }
    }

};

module.exports = prizeHistoryController;
