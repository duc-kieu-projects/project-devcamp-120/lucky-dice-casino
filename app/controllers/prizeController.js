const mongoose = require('mongoose');
const PrizeModel = require('../models/prizeModel');

const PrizeController = {
    //POST
    createPrize: (req,res) => {
        //B1:
        let bodyReq = req.body;
        let newPrize = {
            _id: mongoose.Types.ObjectId(),
            name: bodyReq.name,
            description: bodyReq.description
        };
        //B2: 
        //B3: 
        PrizeModel.create(newPrize, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `ERROR 500: Internal server error : ${err.message}`
                })
            } else {
                return res.status(201).json({
                    status: 'New Prize created!',
                    data
                })
            }
        })
    },

    //GET all prizes
    getAllPrizes: (req,res) => {
        //B1:
        //B2:
        //B3:
        PrizeModel.find( (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `ERROR 500: Internal server error :${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET All Prizes',
                    data
                })
            }
        })
    },

    //GET a prize ID
    getPrizeById: (req,res) => {
        //B1: 
        let prizeId = req.params.prizeId;
        //B2: 
        if (!mongoose.Types.ObjectId.isValid(prizeId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad request',
                message: 'Prize Id is invalid!'
            })
        };
        //B3: 
        PrizeModel.findById(prizeId, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `ERROR 500: Internal server error :${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET Prize Successfully!',
                    data
                })
            }
        })
    },

    //PUT a prize
    updatePrizeById: (req,res) => {
        //B1: 
        let prizeId = req.params.prizeId;
        let bodyReq = req.body;
        let updatePrize = {
            name: bodyReq.name,
            description: bodyReq.description
        };
        //B2:
        //B3:
        PrizeModel.findByIdAndUpdate(prizeId, updatePrize, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `ERROR 500: Internal server error :${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET Prize Successfully!',
                    data,
                    'Data update': updatePrize
                })
            }
        })
    },

    //DELETE prize by ID
    deletePrizeById: (req,res) => {
        //B1: 
        let prizeId = req.params.prizeId;
        //B2: 
        if (!mongoose.Types.ObjectId.isValid(prizeId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad request',
                message: 'Prize Id is invalid!'
            })
        };
        //B3: 
        PrizeModel.findByIdAndDelete(prizeId, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `ERROR 500: Internal server error :${err.message}`
                })
            } else {
                return res.status(204).json({
                    status: 'DEL Prize Successfully!',
                })
            }
        })
    }
};

module.exports = PrizeController;
