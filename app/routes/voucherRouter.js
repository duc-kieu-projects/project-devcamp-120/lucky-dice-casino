const VoucherRouter = require('express').Router();
const VoucherController = require('../controllers/voucherController');

VoucherRouter.post('/vouchers', VoucherController.createVoucher);

VoucherRouter.get('/vouchers', VoucherController.getAllVouchers);

VoucherRouter.get('/vouchers/:voucherId', VoucherController.getVoucherById);

VoucherRouter.put('/vouchers/:voucherId', VoucherController.updateVoucherById);

VoucherRouter.delete('/vouchers/:voucherId', VoucherController.deleteVoucherById);

module.exports = VoucherRouter;
