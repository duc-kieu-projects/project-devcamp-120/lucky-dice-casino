const PrizeHistoryRouter = require('express').Router();
const prizeHistoryController = require('../controllers/prizeHistoryController');

PrizeHistoryRouter.post('/prizes-histories', prizeHistoryController.createPrizeHistory);

PrizeHistoryRouter.get('/prizes-histories', prizeHistoryController.getAllPrizesHistory);

PrizeHistoryRouter.get('/prizes-histories/:prizeHistoryId', prizeHistoryController.getPrizeHistoryById);

PrizeHistoryRouter.put('/prizes-histories/:prizeHistoryId', prizeHistoryController.updatePrizeHistoryById);

PrizeHistoryRouter.delete('/prizes-histories/:prizeHistoryId', prizeHistoryController.deletePrizeHistoryById);



module.exports = PrizeHistoryRouter;
