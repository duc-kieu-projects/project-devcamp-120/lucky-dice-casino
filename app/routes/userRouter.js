const UserRouter = require('express').Router();
const UserController = require('../controllers/userController');

UserRouter.post('/users', UserController.createUser);

UserRouter.get('/users', UserController.getAllUsers);

UserRouter.get('/users/:userId', UserController.getUserById);

UserRouter.put('/users/:userId', UserController.updateUserById);

UserRouter.delete('/users/:userId', UserController.deleteUserById);

module.exports = UserRouter;
