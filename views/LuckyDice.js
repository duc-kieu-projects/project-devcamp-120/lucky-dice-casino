$(document).ready(function(){
    "use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gCOL_DICE_HISTORY = ["index", "dice"];
    const gCOL_VOUCHER_HISTORY = ["voucher.code", "voucher.discount"];
    const gCOL_PRIZE_HISTORY = ["index", "prize.name"];
    const gFIRST_COL = 0;
    const gSECOND_COL = 1;

    const gEND_OF_ROW_COL = -1;
    const gUserObj = {
        username: "",
        firstname: "",
        lastname: ""
    };

    var vStt = 1
// /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    $(document).ready(function() {
        //sự kiện bấm nút ném dice
        $("#btn-dice").click(function() {
            $("#btn-dice").toggleClass("btn-success");
            onBtnNewDice();
        });
        
        //sự kiện bấm dice history
        $("#btn-dice-history").on("click", function() {
            onBtnDiceHistory();
        })

        //sự kiện bấm voucher history
        $("#btn-voucher-history").on("click", function() {
            onBtnVoucherHistory();
        })

        //sự kiện bấm prize history
        $("#btn-present-history").on("click", function() {
            onBtnPrizeHistory();
        })
    })

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // hàm xử lý ném xúc xắc
    function onBtnNewDice() {
        "use strict";
        console.log("%cThrow New Dice!", "color:red");
        //buoc 1: thu thap du lieu
        getData(gUserObj);
        //buoc 2: kiem tra du lieu
        var vCheckData = validateData(gUserObj);
        if ( vCheckData == true ) {
            //buoc 3: send request API & show result
            callAPIGetNewDice(gUserObj);
        }
    }

    // hàm xử lý xem lịch sử ném xúc xắc
    function onBtnDiceHistory() {
        "use strict";
        console.log("%cDice History Clicked!", "color:blue");
        changeColorBtn("Dice");
        //bước 1: thu thập dữ liệu
        getData(gUserObj);
        //bước 2: kiểm tra dữ liệu
        var vCheckData = validateData(gUserObj);
        if ( vCheckData == true ) {
            //bước 3: call API lấy Dice History
            callAPIGetDiceHistory(gUserObj);
        }
    }

    // hàm xem lịch sử voucher
    function onBtnVoucherHistory() {
        "use strict";
        console.log("%cVoucher History Clicked!", "color:red");
        changeColorBtn("Voucher");
        //bước 1: thu thập dữ liệu
        getData(gUserObj);
        //bước 2: kiểm tra dữ liệu
        var vCheckData = validateData(gUserObj);
        if ( vCheckData == true ) {
            //bước 3: call API lấy Voucher History
            callAPIGetVoucherHistory(gUserObj);
        } 
    }

    //hàm xử lý sự kiện hiển thị prize history
    function onBtnPrizeHistory() {
        "use strict";
        console.log("%cPresent History Clicked!", "color:purple");
        changeColorBtn("Present")
        //bước 1: thu thập dữ liệu
        getData(gUserObj);
        //bước 2: kiểm tra dữ liệu
        var vCheckData = validateData(gUserObj);
        if ( vCheckData == true ) {
            //bước 3: call API lấy Prize History
        callAPIGetPrizeHistory(gUserObj);
        }
    }    
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //hàm thu thập dữ liệu
    function getData(paramUserObj) {
        "use strict";
        paramUserObj.username = $("#inp-username").val();
        paramUserObj.firstname = $("#inp-firstname").val();
        paramUserObj.lastname = $("#inp-lastname").val();
    }

    //hàm kiểm tra dữ liệu
    function validateData(paramUserObj) {
        "use strict";
        if ( paramUserObj.username === "" ) {
            alert("username chua nhap!");
            return false;
        }
        
        if ( paramUserObj.firstname === "" ) {
            alert("first name chua nhap!");
            return false;
        }

        if ( paramUserObj.lastname === "" ) {
            alert("last name chua nhap!");
            return false;
        }
        return true;
    }

    //hàm call API tung xúc xắc mới & hiển thị kết quả 
    function callAPIGetNewDice(paramUserObj) {
        "use strict";
        $.ajax({
            url: "/devcamp-lucky-dice/dice",
            type: 'POST',
            contentType: "application/json;charset=UTF-8" ,
            data: JSON.stringify(paramUserObj),
            success: function(responseObj) {
                displayResults(responseObj);
            },
            error: function(errorContent) {
                alert(errorContent.responseText);
            }
        })
    }

    //hàm hiển thị kết quả sau khi tung xúc xắc
    function displayResults(paramResponseObj) {
            console.log(paramResponseObj)
            // get result dice
            var vDiceReady = paramResponseObj.dice;
    
            console.log("dice result:= " + vDiceReady);
               
            //hien thi Xuc Xac
            changeDice(vDiceReady);

            //hien thi loi nhan
            changeNotification(vDiceReady);
                
            //hiển thị voucher
            showVoucher(paramResponseObj);
                
            //hiển thị prize
            showPresent(paramResponseObj);
    }

    //hàm call API lấy voucher history
    function callAPIGetVoucherHistory(paramUserObj) {
        "use strict";
        $.ajax({
            url: "/devcamp-lucky-dice/voucher-history?username=" + paramUserObj.username,
            type: 'GET',
            dataType: 'json',
            success: function(responseObj) {
                console.log(responseObj);
                displayVoucherHistory(responseObj);
            }

        })
    }

    //hàm call API lấy prize history
    function callAPIGetPrizeHistory(paramUserObj) {
        "use strict";
        $.ajax({
            url: "/devcamp-lucky-dice/prize-history?username=" + paramUserObj.username,
            type: 'GET',
            dataType: 'json',
            success: function(responseObj) {
                console.log(responseObj);
                displayPrizeHistory(responseObj);
            }

        })
    }

    // hàm call API lấy Dice History
    function callAPIGetDiceHistory(paramUserObj) {
        "use strict";
        $.ajax({
            url: "/devcamp-lucky-dice/dice-history?username=" + paramUserObj.username,
            type: 'GET',
            dataType: 'json',
            success: function(responseObj) {
                console.log(responseObj);
                displayDiceHistory(responseObj);
            }

        })
    }

    //hàm hiển thị kết quả
    function changeDice(paramDiceReady) {
        "use strict";
        var vNewDice = document.getElementById("img-dice");
        switch (paramDiceReady) {
            case 1:
                vNewDice.src = "LuckyDiceImages/1.png";
                break;
            case 2:
                vNewDice.src = "LuckyDiceImages/2.png";
                break;
            case 3:
                vNewDice.src = "LuckyDiceImages/3.png";
                break;
            case 4:
                vNewDice.src = "LuckyDiceImages/4.png";
                break;
            case 5: 
                vNewDice.src = "LuckyDiceImages/5.png";
                break;
            default:
                vNewDice.src = "LuckyDiceImages/6.png";
        }
    }

//hàm hiển thị lời nhắn
function changeNotification(paramDiceReady) {
    "use strict";
    if ( paramDiceReady < 3 ) {
        $("#p-notification-dice").html("Chúc bạn may mắn lần sau!");
    }
    else {
        $("#p-notification-dice").html("Chúc mừng bạn! Hãy chơi tiếp để thắng lớn!");
    }
}

//hàm hiển thị voucher 
function showVoucher(paramResponse) {
    "use strict";
    if ( paramResponse.voucher != null ) {
        $("#p-voucher-id").html("ID : " + paramResponse.voucher.code)
        $("#p-voucher-percent").html("Phần trăm giảm giá : " + paramResponse.voucher.discount + " %");
    }
    else {
        $("#p-voucher-id").html("");
        $("#p-voucher-percent").html("");
    }
}

//hàm hiển thị present
function showPresent(paramResponse) {
    "use strict";
    if (!paramResponse.prize) {
        $("#img-present").attr("src","LuckyDiceImages/no-present.jpg");
    };
    
    if (paramResponse.prize.name == "Mũ") {
            $("#img-present").attr("src","LuckyDiceImages/hat.jpg");
    };

    if (paramResponse.prize.name == "Xe đạp địa hình") {
        $("#img-present").attr("src","LuckyDiceImages/bike.jpg");
    };

    if (paramResponse.prize.name == "Xe máy vinfast") {
        $("#img-present").attr("src","LuckyDiceImages/xe-may.jpg");
    };

    if (paramResponse.prize.name == "Mercedes G63") {
        $("#img-present").attr("src","LuckyDiceImages/car.jpg");
    };

    if (paramResponse.prize.name == "Áo") {
        $("#img-present").attr("src","LuckyDiceImages/t-shirt.jpg");
    };

   
}

//hàm display dice history
function displayDiceHistory(paramDiceHistory) {
    'use strict' 
    $('#dice-history-table').css('display', '');
    $('#voucher-history-table').css('display', 'none');   
    $('#prize-history-table').css('display', 'none');  
    $('#dice-history-table_wrapper').css('display', '');
    $('#prize-history-table_wrapper').css('display', 'none');
    $('#voucher-history-table_wrapper').css('display', 'none'); 
    var vTableDice = $('#dice-history-table').DataTable({
        columns: [
            {data: gCOL_DICE_HISTORY[gFIRST_COL]},
            {data: gCOL_DICE_HISTORY[gSECOND_COL]},
        ],
        columnDefs: [
            {
                targets: gFIRST_COL,
                render: function (data, type, row, meta) {
                    return meta.row + 1 
                }
            },
        ],
        retrieve: true,
    });
    vTableDice.clear();
    vTableDice.rows.add(paramDiceHistory).draw();
    
}

    // hàm display voucher history
    function displayVoucherHistory(paramVoucherHistory) {
        'use strict'
        $('#dice-history-table_wrapper').css('display', 'none');
        $('#prize-history-table_wrapper').css('display', 'none');
        $('#voucher-history-table_wrapper').css('display', '');
        $('#dice-history-table').css('display', 'none');   
        $('#prize-history-table').css('display', 'none');   
        $('#voucher-history-table').css('display', '');

        var vTableVoucher = $('#voucher-history-table').DataTable({
            columns: [
                {data: gCOL_VOUCHER_HISTORY[gFIRST_COL]},
                {data: gCOL_VOUCHER_HISTORY[gSECOND_COL]},
            ],
            columnDefs: [
                {
                    targets: gSECOND_COL,
                    render: (data, type, row, meta) => data + ' %',
                    className: 'text-success'
                },
            ],
            retrieve: true,
        });
        vTableVoucher.clear();
        vTableVoucher.rows.add(paramVoucherHistory).draw();
    }

    //hàm display prize history
    function displayPrizeHistory(paramPrizeHistory) {
        'use strict'
        $('#dice-history-table').css('display', 'none');   
        $('#voucher-history-table').css('display', 'none');   
        $('#prize-history-table').css('display', '');
        $('#dice-history-table_wrapper').css('display', 'none');
        $('#voucher-history-table_wrapper').css('display', 'none');
        $('#prize-history-table_wrapper').css('display', '');

        var vTablePrize = $('#prize-history-table').DataTable({
            columns: [
                {data: gCOL_PRIZE_HISTORY[gFIRST_COL]},
                {data: gCOL_PRIZE_HISTORY[gSECOND_COL]},
            ],
            columnDefs: [
                {
                    targets: gFIRST_COL,
                    render: function (data, type, row, meta) {
                        return meta.row + 1
                    }
                },
                {
                    targets: gSECOND_COL,
                    className: 'text-primary'
                },
            ],
            retrieve: true,
        });
        vTablePrize.clear();
        vTablePrize.rows.add(paramPrizeHistory).draw();

    }


//hàm đổi màu nút 
function changeColorBtn(paramBtn) {
    "use strict";
    switch(paramBtn) {
        case "Dice":
            $("#btn-dice-history").removeClass("btn-primary");
            $("#btn-dice-history").addClass("btn-warning");
            $("#btn-voucher-history").removeClass("btn-warning");
            $("#btn-voucher-history").addClass("btn-primary");
            $("#btn-present-history").removeClass("btn-warning");
            $("#btn-present-history").addClass("btn-primary");
            break;

        case "Voucher":
            $("#btn-dice-history").addClass("btn-primary");
            $("#btn-dice-history").removeClass("btn-warning");
            $("#btn-voucher-history").addClass("btn-warning");
            $("#btn-voucher-history").removeClass("btn-primary");
            $("#btn-present-history").removeClass("btn-warning");
            $("#btn-present-history").addClass("btn-primary");
            break;

        case "Present":
            $("#btn-dice-history").addClass("btn-primary");
            $("#btn-dice-history").removeClass("btn-warning");
            $("#btn-voucher-history").removeClass("btn-warning");
            $("#btn-voucher-history").addClass("btn-primary");
            $("#btn-present-history").addClass("btn-warning");
            $("#btn-present-history").removeClass("btn-primary");
            break;

    }
}

})