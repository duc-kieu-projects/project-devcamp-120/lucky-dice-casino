const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const port = 8080;
const app = express();

app.use(express.json());
app.use(express.static(path.join(__dirname, 'views')));

//import Router
const userRouter = require('./app/routes/userRouter');
const diceHistoryRouter = require('./app/routes/diceHistoryRouter')
const voucherRouter = require('./app/routes/voucherRouter');
const prizeRouter = require('./app/routes/prizeRouter');
const prizeHistoryRouter = require('./app/routes/prizeHistoryRouter');
const voucherHistoryRouter = require('./app/routes/voucherHistoryRouter');
const diceRouter = require('./app/routes/diceRouter');

app.get('/devcamp-lucky-dice', (req,res) => res.sendFile(path.join(__dirname, '/views/Lucky-Dice.html')));

//Task NR 1.30-40
app.use('/', userRouter);

//Task NR 1.50 
app.use('/', diceHistoryRouter);

//Task NR 1.60 - 70
app.use('/', voucherRouter);

//Task NR 1.80
app.use('/', prizeRouter)

//Task NR 2.
app.use('/', prizeHistoryRouter);
app.use('/', voucherHistoryRouter);

//Task NR 4.
app.use('/devcamp-lucky-dice', diceRouter);

//connect mongoDB 
const uri = 'mongodb://localhost:27017/LuckyDice_CRUD';
mongoose.connect(uri, (error) => {
    if (error) throw error;
    console.log('MongoDB connected');
   })

app.listen(port, () => console.log(`App is listening on port ${port}`));
